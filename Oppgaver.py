import random

# Brukte ChatGPT

def print_command_guide():
    mssg = """
Available commands:
 
    B - Converts a decimal number to binary - Task #2
    D - Converts a binary number to decimal - Task #3
    M - Math practice for multiplications between 0 and 10 - Task #1
    H - Prints this menu.
    Q - Quit, exits this program.
    """
    print(mssg)
    
    
def main():
    print("Welcome to the main menu of this programming task.")
    print_command_guide()
    running = True
    
    while running:
        choise = input("What would you like to do?\n\t> ").upper()
        
        match choise:
            case "B":
                #Oppgave 1! - Definer/lag funksjonen to_binary()
                to_binary()
                
            case "D":
                #Oppgave 2! - Definer/lag funksjonen to_decimal()
                to_decimal()
                
            case "M":
                #Oppgave 3! - Definer/lag funksjonen math_practice()
                multiplication_practice()
                
            case "H":
                print_command_guide()
            case "Q":
                running = False    
            case _:
                print("No such command, enter 'H' for help.")      

def to_binary():
  while True:
    try:
      number = int(input("Skriv inn et heltall: "))
      # Konverter tall til binært
      binary = bin(number)
      # Skriv ut den konverterte verdien
      print(f"{number} i binært er {binary}")
    except ValueError:
      # Dersom brukeren skriver inn en bokstav istedenfor et tall, vil dette unntaket bli fanget opp
      print("Vennligst skriv inn et heltall.")
    else:
      # Dersom ingen unntak oppstår, går vi ut av while-løkken
      break
  # Gå tilbake til hovedmenyen
  main() 
def to_decimal():
  while True:
    try:
      # Be brukeren om å skrive inn et binært tall
      print("Dette 0b10 er 2 i binært og er gyldig")
      binary = input("Skriv inn et binært tall: ")
      # Konverter binært tall til desimaltall
      number = int(binary, 2)
      # Skriv ut den konverterte verdien
      print(f"{binary} i desimaltall er {number}")
    except ValueError:
      # Dersom brukeren skriver inn noe annet enn et gyldig binært tall, vil dette unntaket bli fanget opp
      print("Vennligst skriv inn et gyldig binært tall.")
    else:
      # Dersom ingen unntak oppstår, går vi ut av while-løkken
      break
  # Gå tilbake til hovedmenyen
  main()
import random

def multiplication_practice():
  while True:
    # Velg 2 tilfeldige tall fra 0-10
    num1 = random.randint(0, 10)
    num2 = random.randint(0, 10)
    
    # Gi regnestykket til brukeren
    result = num1 * num2
    user_input = input(f"{num1} * {num2} = ")
    
    # Sjekk om brukeren har skrevet inn "X"
    if user_input.lower() == "x":
      main()
    
    # Konverter brukerens svar til et tall og sjekk om det er riktig
    correct = False
    for _ in range(2):
      try:
        if int(user_input) == result:
          correct = True
          break
      except ValueError:
        pass
      user_input = input("Ugyldig svar. Prøv igjen: ")
    
    # Skriv ut resultatet og gi et nytt regnestykke hvis svar var riktig, ellers gi et nytt regnestykke
    if correct:
      print("Riktig!\n")
    else:
      print(f"Feil. Riktig svar er {result}.\n")


main()